package bancodedados.dao;

import bancodedados.beans.Aluno;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AlunoDAO {
    
    public void Save(Aluno obj) throws SQLException {
        String sql = "insert into alunos (nome, email, telefone, matricula) " 
                    +"values (?,?,?,?)";
        Connection cx = ConnectionFactory.getConnection();
        PreparedStatement st;
        st = cx.prepareStatement(sql);
        try {
            st.setString(0, obj.getNome());
            st.setString(1, obj.getEmail());
            st.setString(2, obj.getTelefone());
            st.setString(3, obj.getMatricula());
            st.execute();
        } catch (Exception e) {
            throw e;
        } finally {
            st.close();
            cx.close();
        }
    }
    
    public void Update(Aluno obj) throws SQLException {
        String sql = "update alunos set nome = ?, email = ?, telefone = ? where matricula = ?;";
        Connection cx = ConnectionFactory.getConnection();
        PreparedStatement st;
        st = cx.prepareStatement(sql);
        try {
            st.setString(0, obj.getNome());
            st.setString(1, obj.getEmail());
            st.setString(2, obj.getTelefone());
            st.setString(3, obj.getMatricula());
        } catch (Exception e) {
            throw e;
        } finally {
            st.execute();
            cx.close();
        }
    }
    
    public void Delete(Aluno obj) throws SQLException {
        String sql = "delete from alunos where id = ?";
        Connection cx = ConnectionFactory.getConnection();
        PreparedStatement st;
        st = cx.prepareStatement(sql);
        try {
            st.setInt(0, obj.getId());
        } catch (Exception e) {
            throw e;
        } finally {
            st.execute();
            cx.close();
        }
    }
     
    public void Delete(int id) throws SQLException {
        String sql = "delete from alunos where id = " + id;
        Connection cx = ConnectionFactory.getConnection();
        PreparedStatement st;
        st = cx.prepareStatement(sql);
        try {
            st.setInt(0, id);
        } catch (Exception e) {
            throw e;
        } finally {
            st.execute();
            cx.close();
        }
    }
    
    public ArrayList<Aluno> List() throws SQLException {
        String sql = "select id, nome, email, telefone, matricula from alunos";
        Connection cx = ConnectionFactory.getConnection();
        Statement st;
        ResultSet rs;
        st = cx.createStatement();
        
        ArrayList<Aluno> list = new ArrayList<Aluno>();
        Aluno aluno;
        
        rs = st.executeQuery(sql);
        while (rs.next()) {
            
            aluno = new Aluno();
            
            aluno.setId(rs.getInt("id"));
            aluno.setNome(rs.getString("nome"));
            aluno.setEmail(rs.getString("email"));
            aluno.setMatricula(rs.getString("matricula"));
            aluno.setTelefone(rs.getString("telefone"));

            list.add(aluno);
        }
        cx.close();
        
        return list;
    }
}
