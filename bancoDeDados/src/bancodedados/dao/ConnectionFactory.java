package bancodedados.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    
    public static Connection getConnection() throws SQLException {
        Connection cx = null;
        try {
            String url = "jdbc:postgresql://localhost/estacio";
            String user = "postgres";
            String pwd = "123456";
            cx = DriverManager.getConnection(url, user, pwd);
            return cx;
        } catch (SQLException e) {
            throw e;
        }
    }
}

