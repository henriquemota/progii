package bancodedados.repository;

import bancodedados.beans.Aluno;
import bancodedados.dao.AlunoDAO;

public class AlunoREP {
    
    private AlunoDAO alunoDAO ;
    
    public Aluno gravar(Aluno obj) throws Exception {
        
        if (obj.getEmail().trim().length() == 0)
            throw new Exception("Email é obrigatório");
        
        alunoDAO.Save(obj);
        return obj;
    }
    
}
