package bancodedados.view;

import bancodedados.beans.Aluno;
import bancodedados.repository.AlunoREP;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AlunoForm extends Base {
    
    JLabel[] labels;
    JTextField[] texts;
    JButton[] buttons;
    Aluno aluno;
    
    public AlunoForm() {
        super();
        this.initializeForm();
        this.setVisible(true);
    }
    
    private void initializeForm() {
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 5,5));
        
        labels = new JLabel[4];
        texts = new JTextField[4];
        buttons = new JButton[2];
        
        for (int i = 0; i < labels.length; i++) {
            labels[i] = new JLabel();
        }
        
        for (int i = 0; i < texts.length; i++) {
            texts[i] = new JTextField();
            texts[i].setColumns(20);
        }
        
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton();
        }
        
        labels[0].setText("Nome");
        labels[1].setText("Matricula");
        labels[2].setText("Email");
        labels[3].setText("Telefone");
        
        texts[0].setActionCommand("Nome");
        texts[1].setActionCommand("Matricula");
        texts[2].setActionCommand("Email");
        texts[3].setActionCommand("Telefone");
        
        buttons[0].setText("Gravar");
        buttons[0].setActionCommand("Gravar");
        
        buttons[1].setText("Limpar");
        buttons[1].setActionCommand("Limpar");
        
        for (int i = 0; i < 4; i++) {
            this.add(labels[i]);
            this.add(texts[i]);
        }
        
        this.add(buttons[0]);
        this.add(buttons[1]);
    }
    
    private class handleAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            
            aluno = new Aluno();
            aluno.setNome(texts[0].getText());
            aluno.setMatricula(texts[1].getText());
            aluno.setEmail(texts[2].getText());
            aluno.setTelefone(texts[3].getText());
            
            if (e.getActionCommand() == "Gravar") {
                try {
                    new AlunoREP().gravar(aluno);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }
    
}
