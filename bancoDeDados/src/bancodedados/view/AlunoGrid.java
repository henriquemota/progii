/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancodedados.view;

import bancodedados.beans.Aluno;
import bancodedados.dao.AlunoDAO;
import java.awt.FlowLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;

/**
 *
 * @author henriquemota
 */
public class AlunoGrid extends Base{
    
    JTable grid;
    AlunoDAO alunoDAO;
    
    public AlunoGrid() throws SQLException {
        super();
        this.initializeForm();
        this.setVisible(true);
    }
    
    private void initializeForm() throws SQLException {
        
        alunoDAO = new AlunoDAO();
        
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 5,5));
        
        //String[] colunas = {"id", "nome", "email", "telefone", "matricula"};
        String[] colunas = {"nome", "telefone","email"};
        Object [][] dados = {
                                {"Ana Monteiro", "48 9923-7898", "ana.monteiro@gmail.com"},
                                {"João da Silva", "48 8890-3345", "joaosilva@hotmail.com"},
                                {"Pedro Cascaes", "48 9870-5634", "pedrinho@gmail.com"}
                            };
        grid = new JTable(dados, colunas);
        this.add(grid);
        
    }
    
}
