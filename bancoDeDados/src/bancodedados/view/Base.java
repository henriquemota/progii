package bancodedados.view;

import javax.swing.JFrame;

public abstract class Base extends JFrame {
    
    public Base() {
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
    }
    
}
