package calculadora;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class FramePrincipal extends BaseFrame {
    
    private JPanel painelSuperior, painelEsquerda, painelDireita;
    private JTextField textResultado;
    private JButton[] botoesNumeros;
    private JButton[] botoesComandos;
    private String ope;
    private double mem;
    
    public FramePrincipal() {
        // chama o construtor da classe base
        super("Calculadora", 300, 300);
        
        // instancia um objeto de layout e seta (define) para 
        // a janela atual
        BorderLayout layout = new BorderLayout();
        this.setLayout(layout);
        this.setResizable(false);
        
        this.inicializacao();
    }
    
    private void inicializacao() {
        // instancia os objetos de paineis
        this.inicializarPainelSuperior();        
        this.inicializarPainelEsquerda();
        this.inicializarPainelDireita();
        
        // limpa a memoria
        ope = "";
        mem = 0;
    }
    
    private void inicializarPainelSuperior() {
        // instancia o objeto do painel e define o layout
        this.painelSuperior = new JPanel();
        this.painelSuperior.setLayout(new FlowLayout());
        
        // instancia o objeto de text
        this.textResultado = new JTextField(15);
        this.textResultado.setFont(new Font("Verdana", Font.BOLD, 18));
        this.textResultado.setEnabled(false);
        this.painelSuperior.add(this.textResultado);  
        
        this.add("North", this.painelSuperior);
    }
    
    private void inicializarPainelEsquerda() {
            // instancia o objeto do painel e define o layout
        this.painelEsquerda = new JPanel();
        this.painelEsquerda.setLayout(new GridLayout(5, 3));
        
        // intancia os botoes
        this.botoesNumeros = new JButton[14];
        this.botoesNumeros[0] = new JButton("C");
        this.botoesNumeros[0].setActionCommand("C");
        this.botoesNumeros[0].addActionListener(new MyActionClass());

        this.botoesNumeros[1] = new JButton("CE");
        this.botoesNumeros[1].setActionCommand("CE");
        this.botoesNumeros[1].addActionListener(new MyActionClass());

        this.botoesNumeros[2] = new JButton("+/-");
        this.botoesNumeros[2].setActionCommand("+/-");
        this.botoesNumeros[2].addActionListener(new MyActionClass());
        
        for (int i = 3; i < 13; i++) {
            this.botoesNumeros[i] = new JButton(Integer.toString(12-i));
            this.botoesNumeros[i].setActionCommand(Integer.toString(12-i));
            this.botoesNumeros[i].addActionListener(new MyActionClass());
        }
        
        this.botoesNumeros[13] = new JButton(",");
        this.botoesNumeros[13].setActionCommand(",");
        this.botoesNumeros[13].addActionListener(new MyActionClass());

        this.incluirBotoesContainer(this.botoesNumeros, this.painelEsquerda, "West");
    }
    
    private void inicializarPainelDireita() {
            // instancia o objeto do painel e define o layout
        this.painelDireita = new JPanel();
        this.painelDireita.setLayout(new GridLayout(5,1));
        
        // intancia os botoes
        this.botoesComandos = new JButton[5];
        this.botoesComandos[0] = new JButton("+");
        this.botoesComandos[1] = new JButton("-");
        this.botoesComandos[2] = new JButton("*");
        this.botoesComandos[3] = new JButton("/");
        this.botoesComandos[4] = new JButton("=");
        
        for (JButton botao : this.botoesComandos) {
            botao.setActionCommand(botao.getText());
            botao.addActionListener(new MyActionClass());
        }
        
        this.incluirBotoesContainer(this.botoesComandos, this.painelDireita, "East");
    }
    
    private void incluirBotoesContainer(JButton[] botoes, JPanel painel, String local) {
        
        for (JButton botao : botoes) {
            painel.add(botao);
        }
        
        this.add(local, painel);
    }
    
    private void setNumbers(String num) {
        String valor = textResultado.getText();
        if (",".equals(num) && valor.contains(num)) return;
        textResultado.setText(valor + num);
    }
    
    private void setOperators(String num) {
        if ("C".equals(num)) {
            textResultado.setText("");
        } else if ("CE".equals(num)) {
            textResultado.setText("");
            ope = "";
            mem = 0;
        } else if ("+-*/".contains(num)) {
            String valor = textResultado.getText().replace(',', '.');
            textResultado.setText("");
            ope = num;
            mem = Double.parseDouble(valor);
        } else if ("+/-".contains(num)) {
            String valor = textResultado.getText().replace(',', '.');
            System.out.print(valor);
            if (valor.length()<=0) return;
            else if ("-".contains(valor)) {
                textResultado.setText(valor.replace("-", ""));
            }
            else {
                textResultado.setText("-" + valor);
            }
        }
        else {
            Double result = 0.0;
            Double valor = Double.parseDouble(textResultado.getText().replace(',', '.'));
            switch (ope) {
                case "+": 
                    result = mem + valor;
                    break;
                case "-": 
                    result = mem - valor;
                    break;
                case "*": 
                    result = mem * valor;
                    break;
                case "/": 
                    result = mem / valor;
                    break;
            }
            textResultado.setText(result.toString().replace('.', ','));
        }
    }
    
    class MyActionClass implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String nums = "0123456789,";
            String cmd = e.getActionCommand();
            
            if (nums.contains(cmd)) {
                setNumbers(cmd);
            } else {
                setOperators(cmd);
            } 
        }
    
    }
}
