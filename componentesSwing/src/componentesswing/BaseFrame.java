package componentesswing;

import java.awt.FlowLayout;
import javax.swing.JFrame;

public abstract class BaseFrame extends JFrame {
    
    public BaseFrame() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600,400);
        this.setLayout(new FlowLayout());
    }
}
