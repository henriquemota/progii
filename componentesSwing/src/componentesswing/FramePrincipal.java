package componentesswing;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FramePrincipal extends BaseFrame {
    
    JTextField txLogin, txSenha;
    JLabel lbAjuda;
    
    public FramePrincipal() {
        super();
        init();
    }
    
    public void init() {
        txLogin = new JTextField(10);
        txLogin.setToolTipText("Informe o email para login");
        txLogin.addFocusListener(new MyFocusListener());

        txSenha = new JTextField(10);
        txSenha.setToolTipText("Informe a senha forte para login");
        txSenha.addFocusListener(new MyFocusListener());
        
        this.add(txLogin);
        this.add(txSenha);
        
        lbAjuda = new JLabel();
        this.add(lbAjuda);
    }
    
    class MyFocusListener implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            String msg =  ((JTextField)e.getComponent()).getToolTipText();
            lbAjuda.setText(msg);
        }

        @Override
        public void focusLost(FocusEvent e) {
            lbAjuda.setText("");
        }
    }
    
    
}
