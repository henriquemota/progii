package componentesswing;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FrameSimples extends BaseFrame {
    
    JLabel lbUsuario, lbSenha, lbImg;
    JTextField txUsuario, txSenha;
    JButton btLogin;
    
    public FrameSimples() {
        this.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.init();
    }
    
    public void init() {
        lbImg = new JLabel(new ImageIcon("assets/img/pwd.png"));
        lbUsuario = new JLabel("Usuário para login");
        lbSenha = new JLabel("Senha para login");
        
        txUsuario = new JTextField(20);
        txUsuario.setToolTipText("Informe seu email");
        txSenha = new JTextField(20);
        
        btLogin = new JButton("Login");
        
        this.add(lbImg);
        this.add(lbUsuario);
        this.add(txUsuario);
        this.add(lbSenha);
        this.add(txSenha);
        this.add(btLogin);
        
        this.pack();
    }
    
}
