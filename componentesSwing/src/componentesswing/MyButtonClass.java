package componentesswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class MyButtonClass extends BaseFrame {
    
    JButton btConfirm, btCancel;
    
    public MyButtonClass () {
        initialize();
    }
    
    private void initialize() {
        btConfirm = new JButton();
        btCancel = new JButton();

        MyActionClass obj1, obj2;
        obj1 = new MyActionClass();
        obj2 = new MyActionClass();
        
        btConfirm.setText("Confirmar");
        btConfirm.setIcon(new ImageIcon("assets/img/check.jpg"));
        btConfirm.setActionCommand("CONFIRMAR");
        btConfirm.addActionListener(obj1);

        btCancel.setText("Cancelar");
        btCancel.setIcon(new ImageIcon("assets/img/cancel.jpg"));
        btCancel.setActionCommand("CANCELAR");
        btCancel.addActionListener(obj2);
        
        this.add(btConfirm);
        this.add(btCancel);
    }
    
    class MyActionClass implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(e.getActionCommand());
            if ("CONFIRMAR".equals(e.getActionCommand())) {
                btConfirm.setEnabled(false);
            } else if ("CANCELAR".equals(e.getActionCommand())) {
                btConfirm.setEnabled(true);
            }
        }
        
    }
    
}
