package views;

import javax.swing.JFrame;

public abstract class Base extends JFrame {
    
    public Base() {
        initialize();
    }
    
    private void initialize() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);        
        this.setResizable(false);
    }
    
}
