package views;

import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Login extends Base {
    
    JLabel lbImg, lbLogin, lbSenha;
    JTextField txLogin;
    JPasswordField txSenha;
    JButton btLogin, btLimpar;
    
    public Login(){
        super();
        initialize();
    }
    
    private void initialize(){
        this.setLayout(new FlowLayout());
        this.setSize(250, 400);
        this.setBackground(Color.WHITE);
        
        
        lbImg = new JLabel(new ImageIcon("assets/img/cadeado.png"));
        lbLogin = new JLabel("Login");
        lbSenha = new JLabel("Senha");
        
        txLogin = new JTextField(20);
        txSenha = new JPasswordField(20);
        
        btLogin = new JButton("Login");
        btLimpar = new JButton("Limpar");
        
        this.add(lbImg);
        this.add(lbLogin);
        this.add(txLogin);
        this.add(lbSenha);
        this.add(txSenha);
        this.add(btLogin);
        this.add(btLimpar);
        
    }
    
}
