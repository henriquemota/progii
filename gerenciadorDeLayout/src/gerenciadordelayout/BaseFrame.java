package gerenciadordelayout;

import javax.swing.JFrame;

/**
 *
 * @author henriquemota
 */ 
public class BaseFrame extends JFrame {
    
    public BaseFrame() {
        this("PROG II");
    }
    
    public BaseFrame(String title) {
        this(title, 400, 300);
    }
    
    public BaseFrame(String title, int width, int height) {
        this.setTitle(title);
        this.setSize(width, height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }
    
}
