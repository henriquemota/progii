package gerenciadordelayout;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author henriquemota
 */
public class FrameBorderLayout extends BaseFrame {
    protected JPanel painelSuperior, painelCentro;
    
    public FrameBorderLayout() {
        super();
        this.initialize();
        this.setVisible(true);
    }
    
    private void initialize() {
        // Definicao do layout no JFrame
        this.setLayout(new BorderLayout());
        
        // Definicao do layout para os paineis
        this.painelSuperior = new JPanel();
        this.painelSuperior.setLayout(new FlowLayout());
        this.painelCentro = new JPanel();
        this.painelCentro.setLayout(new GridLayout(2, 2));
        
        this.painelSuperior.add(new JButton("Botao superior 1"));
        this.painelSuperior.add(new JButton("Botao superior 2"));
        
        this.painelCentro.add(new JButton("Botao centro 1"));
        this.painelCentro.add(new JButton("Botao centro 2"));
        this.painelCentro.add(new JButton("Botao centro 3"));

        // Inclusao dos paineis no JFrame
        this.add("North", painelSuperior);
        this.add("Center", painelCentro);
        
       
    }    
    
}
