package gerenciadordelayout;

import java.awt.Container;
import javax.swing.*;

/**
 *
 * @author henriquemota
 */
public class FrameBoxSimples extends BaseFrame {
    private static final int NUM_BOTOES = 3;
    protected JButton botoes[];
    
    public FrameBoxSimples() {
        super();
        this.initialize();
        this.setVisible(true);
    }
    
    private void initialize() {
        JPanel painel = new JPanel();
        BoxLayout layout = new BoxLayout(painel, BoxLayout.X_AXIS);
        painel.setLayout(layout);
        
        this.botoes = new JButton[NUM_BOTOES];
        for (int i = 0; i < this.botoes.length; i++) {
            this.botoes[i] = new JButton("Botão " + i);
            painel.add(this.botoes[i]);
        }
        Container c = this.getContentPane();
        c.add(painel);
    }
    
}
