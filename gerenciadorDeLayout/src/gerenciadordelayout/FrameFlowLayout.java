package gerenciadordelayout;

import java.awt.FlowLayout;
import javax.swing.JButton;

/**
 *
 * @author henriquemota
 */
public class FrameFlowLayout extends BaseFrame {
    
    private static final int NUM_BOTOES = 10;
    protected JButton botoes[];
    
    public FrameFlowLayout() {
        super();
        this.initialize();
        this.setVisible(true);
    }
    
    private void initialize() {
        FlowLayout layout = new FlowLayout(FlowLayout.RIGHT, 2, 2);
        this.setLayout(layout);
        
        this.botoes = new JButton[NUM_BOTOES];
        for (int i = 0; i < this.botoes.length; i++) {
            this.botoes[i] = new JButton("Botão " + i);
            this.add(this.botoes[i]);
        }
    }    
}
