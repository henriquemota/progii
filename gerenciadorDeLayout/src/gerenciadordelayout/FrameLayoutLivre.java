package gerenciadordelayout;

import java.awt.BorderLayout;
import javax.swing.JButton;

/**
 *
 * @author henriquemota
 */
public class FrameLayoutLivre extends BaseFrame {
    private static final int NUM_BOTOES = 3;
    protected JButton botoes[];
    
    public FrameLayoutLivre() {
        super();
        this.initialize();
        this.setVisible(true);
    }
    
    private void initialize() {
        this.setLayout(null);
        
        this.botoes = new JButton[NUM_BOTOES];
        int x = 20;
        for (int i = 0; i < this.botoes.length; i++) {
            this.botoes[i] = new JButton("Botão " + i);
            this.botoes[i].setBounds(10, x, 200, 40);
            this.add(this.botoes[i]);
            x = x + 60;
        }

    }    
    
}
