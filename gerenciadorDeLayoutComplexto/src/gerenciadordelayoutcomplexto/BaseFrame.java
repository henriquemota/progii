package gerenciadordelayoutcomplexto;

import javax.swing.JFrame;

public abstract class BaseFrame extends JFrame {
    
    public BaseFrame() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
}
