package gerenciadordelayoutcomplexto;

import javax.swing.BoxLayout;
import javax.swing.JButton;

public class BoxLayoutDemo extends BaseFrame {
    
    public BoxLayoutDemo() {
        super();
        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        
        for (int i = 0; i < 10; i++) {
            add(new JButton("Botao"));
        }
        
        setSize(400,400);
        setVisible(true);
    }
    
}
