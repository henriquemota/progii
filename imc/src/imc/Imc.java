package imc;

import javax.swing.JOptionPane;

/**
 *
 * @author henriquemota
 */
public class Imc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        initialize();
    }
    
    private static void initialize() {
        String nome = JOptionPane.showInputDialog(null, "Informe seu nome", "Atenção", JOptionPane.INFORMATION_MESSAGE);
        double peso = parseDouble(JOptionPane.showInputDialog(null, "Informe seu peso", "Atenção", JOptionPane.INFORMATION_MESSAGE));
        double altura = parseDouble(JOptionPane.showInputDialog(null, "Informe sua altura", "Atenção", JOptionPane.INFORMATION_MESSAGE));
        
        switch (Saude.CalcularIMC(peso, altura)) {
            case Saude.ABAIXO_PESO:
                JOptionPane.showMessageDialog(null, nome + ", você está abaixo do peso", "Aviso", JOptionPane.WARNING_MESSAGE);
                break;
            case Saude.PESO_NORMAL:
                JOptionPane.showMessageDialog(null, nome + ", você está com peso normal", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                break;
            case Saude.ACIMA_PESO:
                JOptionPane.showMessageDialog(null, nome + ", você está acima do peso", "Aviso", JOptionPane.WARNING_MESSAGE);
                break;
            case Saude.OBESIDADE_I:
                JOptionPane.showMessageDialog(null, nome + ", você está com obseidade nível I", "Aviso", JOptionPane.WARNING_MESSAGE);
                break;
            case Saude.OBESIDADE_II:
                JOptionPane.showMessageDialog(null, nome + ", você está com obseidade nível II", "Aviso", JOptionPane.ERROR_MESSAGE);
                break;
            case Saude.OBESIDADE_III:
                JOptionPane.showMessageDialog(null, nome + ", você está com obseidade nível III", "Aviso", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }
    
    static final Double parseDouble(String x) {
        Double a;
        try {
            a = Double.parseDouble(x);
        } catch (NumberFormatException e) {
            a = (double) 0;
        }
        return a;
    }
    
}
