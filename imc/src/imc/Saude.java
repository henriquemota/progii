package imc;

/**
 *
 * @author henriquemota
 */
public class Saude {
    
    public static final int ABAIXO_PESO = 0; 
    public static final int PESO_NORMAL = 1; 
    public static final int ACIMA_PESO = 2; 
    public static final int OBESIDADE_I = 3; 
    public static final int OBESIDADE_II = 4; 
    public static final int OBESIDADE_III = 5; 
    
    static int CalcularIMC(double peso, double altura) {
        double imc = peso / Math.pow(altura, 2);
        if (imc < 18.5)
            return Saude.ABAIXO_PESO;
        if (imc >= 18.5 && imc <=24.99)
            return Saude.PESO_NORMAL;
        if (imc > 24.99 && imc <=29.99)
            return Saude.ACIMA_PESO;
        if (imc > 29.99 && imc <=34.99)
            return Saude.OBESIDADE_I;
        if (imc > 34.99 && imc <=39.99)
            return Saude.OBESIDADE_II;
        
        return  Saude.OBESIDADE_III;
    }
    
}
