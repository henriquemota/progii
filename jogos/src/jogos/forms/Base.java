package jogos.forms;

import java.awt.BorderLayout;
import javax.swing.JFrame;

public abstract class Base extends JFrame {
    
    public Base(String title) {
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle(title);
    }
    
}
