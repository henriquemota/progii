package jogos.forms;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public final class JogoVelha extends Base {
    
    JPanel pnComandos, pnTabuleiro;
    JLabel lbJogador;
    JButton[] btTabuleiro;
    JButton btNovoJogo;
    
    public JogoVelha() {
        super("Jogo da velha");
        setResizable(false);
        inicializar();
    }
    
    protected void inicializar() {
        pnComandos = new JPanel(new FlowLayout());
        pnTabuleiro = new JPanel(new GridLayout(3,3));
        
        lbJogador = new JLabel("X");
        
        HandleAction escutador = new HandleAction();
        
        btNovoJogo = new JButton("Novo jogo");
        btNovoJogo.setActionCommand("newgame");
        btNovoJogo.addActionListener(escutador);
        
        pnComandos.add(lbJogador);
        pnComandos.add(btNovoJogo);
        
        btTabuleiro = new JButton[9];
        for(int i=0; i<btTabuleiro.length; i++) {
            btTabuleiro[i] = new JButton();
            btTabuleiro[i].setActionCommand("op" +i);
            btTabuleiro[i].addActionListener(escutador);
            btTabuleiro[i].setText("livre");
            pnTabuleiro.add(btTabuleiro[i]);
        }
        
        add("North", pnComandos);
        add("Center", pnTabuleiro);
        
        pack();
    }
    
    class HandleAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if ("newgame".equals(e.getActionCommand()))
                resetGame();
            else
                changeText((JButton)e.getSource());
        }
        
        protected void changeText(JButton bt) {
            bt.setText(lbJogador.getText());
            bt.setEnabled(false);
            if ("X".equals(lbJogador.getText()))
                lbJogador.setText("0");
            else
                lbJogador.setText("X");
            
            //verifica o vencedor
            String win = checkWin();
            if (!"".equals(win))
                JOptionPane.showMessageDialog(null, win);
        }
        
        protected void resetGame() {
            lbJogador.setText("X");
            for (JButton bt : btTabuleiro) {
                bt.setText("livre");
                bt.setEnabled(true);
            }
        }
        
        protected String checkWin() {
            String[] cols = new String[]{"", "", ""};
            String[] rows = new String[]{"", "", ""};
            String diag1 = "";
            String diag2 = "";
            
            for (JButton bt : btTabuleiro) {
                switch (bt.getActionCommand())
                {
                    case "op0":
                    case "op3":
                    case "op6":
                        if (!"livre".equals(bt.getText()))
                            cols[0] += bt.getText();
                     break;
                    case "op1":
                    case "op4":
                    case "op7":
                        if (!"livre".equals(bt.getText()))
                            cols[1] += bt.getText();
                     break;
                    case "op2":
                    case "op5":
                    case "op8":
                        if (!"livre".equals(bt.getText()))
                            cols[2] += bt.getText();
                     break;
                }
                
                switch (bt.getActionCommand())
                {
                    case "op0":
                    case "op1":
                    case "op2":
                        if (!"livre".equals(bt.getText()))
                            rows[0] += bt.getText();
                     break;
                    case "op3":
                    case "op4":
                    case "op5":
                        if (!"livre".equals(bt.getText()))
                            rows[1] += bt.getText();
                     break;
                    case "op6":
                    case "op7":
                    case "op8":
                        if (!"livre".equals(bt.getText()))
                            rows[2] += bt.getText();
                     break;
                }
                
                switch (bt.getActionCommand())
                {
                    case "op0":
                    case "op8":
                        if (!"livre".equals(bt.getText()))
                            diag1 += bt.getText();
                     break;
                    case "op2":
                    case "op6":
                        if (!"livre".equals(bt.getText()))
                            diag2 += bt.getText();
                     break;
                    case "op4":
                        if (!"livre".equals(bt.getText())) {
                            diag1 += bt.getText();
                            diag2 += bt.getText();
                        }
                     break;
                }
            }
            
            if (diag1.indexOf("XXX") >=0 || diag2.indexOf("XXX") >=0)
                return "Vencedor X";
            else if (diag1.indexOf("000") >=0 || diag2.indexOf("000") >=0)
                return "Vencedor 0";
            
            for (String col : cols) {
                if (col.indexOf("XXX") >=0)
                    return "Vencedor X";
                else if (col.indexOf("000")>=0)
                    return "Vencedor 0";
            }
            
            for (String row : rows) {
                if (row.indexOf("XXX") >=0)
                    return "Vencedor X";
                else if (row.indexOf("000") >=0)
                    return "Vencedor 0";
            }
            
            return "";
            
        }
    }
}
