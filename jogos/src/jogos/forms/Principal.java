package jogos.forms;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class Principal extends Base {
    
    JButton[] botoes;
    
    public Principal() {
        super("Tabuleiro de jogos");
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLayout(new FlowLayout());
        setSize(800,600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        inicializar();
    }
    
    private void inicializar() {
        botoes = new JButton[1];
        
        botoes[0] = new JButton("Jogo da velha");
        botoes[0].setActionCommand("jogodavelha");
        botoes[0].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new JogoVelha().setVisible(true);
            }
        });
        
        for (JButton botao : botoes) {
            add(botao);
        }
        
    }
}
