package manipulacaoEventos;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public final class Form1 extends JFrame{
	
	JButton btn, btn1;

	public Form1() {
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);
		this.setLayout(new FlowLayout());
		
		btn = new JButton("Botao de acao");
		btn.setActionCommand("COMANDO1");
		btn.addActionListener(new MyActionListener());
		
		
		btn1 = new JButton("Botao de acao 2");
		btn1.setActionCommand("COMANDO2");
		btn1.addActionListener(new MyActionListener());
			
		this.add(btn);
		this.add(btn1);
	}
	
	class MyActionListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(null, e.getActionCommand());
		}
		
	}
	
}
