package memoria;

import javax.swing.JFrame;

public abstract class BaseFrame extends JFrame {
    
    public BaseFrame() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 600);
        this.setLocationRelativeTo(null);
    }
    
}
