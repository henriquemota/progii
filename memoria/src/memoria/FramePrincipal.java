package memoria;

import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class FramePrincipal extends BaseFrame {
    
    JLabel[] imagens;
    int tamanho;
    
    public FramePrincipal(int t) {
        super();
        if (t > 4)
            this.setSize(800,800);
        else
            this.setSize(600,600);
        
        this.setLayout(this.setTabuleiro(t));

        this.initialize();
    }
    
    public GridLayout setTabuleiro(int t) {
        this.tamanho = t;
        if (t < 2 || t > 5) {
            this.tamanho = 2;
            JOptionPane.showMessageDialog(null, t + " não é um valor válido. O tabluleiro foi inicializado com o tamanho default.");
            return new GridLayout(2, 2);
        }
            
        return new GridLayout(t, 2);
    }
    
    public void initialize() {
        String basePath = "assets/img/";
        this.imagens = new JLabel[this.tamanho * 2];        
        
        ImageIcon img;
        JLabel labImg;
        for (int i = 0; i < this.imagens.length; i++) {
            img = new ImageIcon(basePath + ((i%this.tamanho) + 1) + ".jpg");
            labImg = new JLabel(img);
            this.imagens[i] = labImg;
            this.add(this.imagens[i]);            
        }
    }
    
    private boolean ehPrimo(int numero) {
        for (int j = 2; j < numero; j++) {
            if (numero % j == 0)
            return false;   
        }
        return true;
    }
    
}
