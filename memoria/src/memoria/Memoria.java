package memoria;

import javax.swing.JOptionPane;

public class Memoria {

    public static void main(String[] args) {
        int t = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o tamanho do tabuleiro entre 2 e 5"));
        new FramePrincipal(t).setVisible(true);
    }
    
}
