package paralelismo;

public class Paralelismo {

    public static void main(String[] args) {
        
        /*
        ParalelismoPorHeranca p1 = new ParalelismoPorHeranca("Processo 01", 1, 10);
        ParalelismoPorHeranca p2 = new ParalelismoPorHeranca("Processo 02", 1, 10);
        
        p1.start();
        p2.start();
        */
        /*
        ParalelismoPorInterface p3 = new ParalelismoPorInterface("C", 1, 10);
        ParalelismoPorInterface p4 = new ParalelismoPorInterface("D", 11, 20);
        
        Thread t1 = new Thread(p3);
        Thread t2 = new Thread(p4);
        
        t1.start();
        t2.start();
        */
        
        ParalelismoWaitA a = new ParalelismoWaitA();
        new Thread(a).start();
        
        
        System.out.println("Finalizado o app");
    }
    
}
