package paralelismo;

public class ParalelismoWaitA implements Runnable {
    
    @Override
    public void run() {
        ParalelismoWaitB b = new ParalelismoWaitB();
        new Thread(b).start();
        //System.out.println("Total é igual a: " + b.total);

        synchronized(b){
            try{
                System.out.println("Aguardando o b completar...");
                b.wait();
            }catch(InterruptedException e){
                System.out.println(e.getMessage());
            }

            System.out.println("Total é igual a: " + b.total);
        }
    }
}
